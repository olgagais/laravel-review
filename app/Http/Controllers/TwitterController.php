<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tweet;
use App\Models\User;

class TwitterController extends Controller
{
    public function index()
    {
        $tweetList = Tweet::orderBy('id', 'desc')->get();

        $user = request()->user();
        $headerImage = $user ? $user->headerImage : 'https://www.warrenphotographic.co.uk/photography/bigs/03010-Cute-grey-and-white-kittens-and-rabbit-white-background.jpg';

        return view('twitter', [
            'user' => $user,
            'headerImage' => $headerImage,
            'tweetList' => $tweetList
        ]);
    }

    public function postTweet()
    {
        $request = request();

        $this->validate($request, [
            'content' => 'required|max:20'
        ]);

        $tweet = new Tweet;
        // $_POST['content']
        $tweet->content = $request->input('content');
        $tweet->user_id = $request->user()->id;
        $tweet->save();

        return redirect('/');
    }

    public function getUserTweets($userId)
    {
        $user = User::find($userId);
        $tweetList = Tweet::where('user_id', $userId)
            ->orderBy('id', 'desc')
            ->get();

        return view('twitter', [
            'user' => request()->user(),
            'tweetList' => $tweetList
        ]);
    }
}
