<?php if(Auth::check()) { ?>
    <form action="/tweet" method="post">
        <?php echo csrf_field() ?>
        <div class="<?php echo $errors->has('content') ? 'error' : '' ?>">
            <textarea name="content" rows="8" class="width-100"></textarea>
            <span><?php echo $errors->first('content') ?></span>
        </div>
        <input type="submit" name="submit" value="Tweet!">
    </form>
<?php } ?>

<ul>
    <?php foreach($tweetList as $tweet) { ?>
        <li>
            <?php echo $tweet->content ?>
            — <a href="/user/<?php echo $tweet->user->id ?>"><?php echo $tweet->user->name ?></a>
        </li>
    <?php } ?>
</ul>
