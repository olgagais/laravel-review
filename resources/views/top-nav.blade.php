<div class="padding-10 flex-container">
    <a href="/">Home</a>
    <div class="flex-1"></div>
    <?php if(Auth::check()) { ?>
        <span class="margin-right-10">Hello, <?php echo $user->name ?></span>
        @include('logout-button')
    <?php } else { ?>
        <div class="">
            <a href="/login" class="padding-right-10">Login</a>
            <a href="/register">Register</a>
        </div>
    <?php } ?>
</div>
